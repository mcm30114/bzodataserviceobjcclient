###BZOData Class Definitions
#*BZODataRequest*
The BZODataRequest class handles the communication with a WCF Data Service

##Instance Methods

*submitGETRequest:*

	-(void) submitGETRequest:(NSString*)servicePath;
- servicePath -String representation of the REST URL
- Publishes results to **BZDataRequestDelegate**

*submitPOSTRequest:httpBodyInfo:*

	- (void) submitPOSTRequest:(NSString*)servicePath httpBodyInfo:(NSDictionary*)httpBodyInfo;
- servicePath - String representation of the REST URL
- httpBodyInfo - Name/Value pairs for the type being POSTed
- Publishes results to **BZDataRequestDelegate**


*submitMERGERequest:httpBodyInfo:*

	- (void) submitMERGERequest:(NSString*)servicePath httpBodyInfo:(NSDictionary*)httpBodyInfo;
- servicePath - String representation of the REST URL
- httpBodyInfo - Name/Value pairs for the type being MERGEed
- Publishes results to **BZDataRequestDelegate**

*submitDELETERequest:*

	- (void) submitDELETERequest:(NSString*)servicePath;
- servicePath - String representation of the REST URL
- Publishes any results to **BZDataRequestDelegate**

*submitGETRequest:withCompletionBlock:withFailedBlock:*
	
	- (void) submitGETRequest:(NSString*)servicePath withCompletionBlock:(dataRequestCompleted_t)completionBlock withFailedBlock:(dataRequestFailed_t)failedBlock;
- servicePath -String representation of the REST URL
- withCompletionBlock - **dataRequestCompleted_t** that is called when the request completes
- withFailedBlock - **dataRequestFailed_t** that is called if the request fails


*submitPOSTRequest:httpBodyInfo:withCompletionBlock:withFailedBlock:*

	- (void) submitPOSTRequest:(NSString*)servicePath httpBodyInfo:(NSDictionary*)httpBodyInfo withCompletionBlock:(dataRequestCompleted_t)completionBlock withFailedBlock:(dataRequestFailed_t)failedBlock;
- servicePath - String representation of the REST URL
- httpBodyInfo - Name/Value pairs for the type being POSTed
- withCompletionBlock - **dataRequestCompleted_t** that is called when the request completes
- withFailedBlock - **dataRequestFailed_t** that is called if the request fails


*submitMERGERequest:httpBodyInfo:withCompletionBlock:withFailedBlock:*

	- (void) submitMERGERequest:(NSString *)servicePath httpBodyInfo:(NSDictionary *)httpBodyInfo withCompletionBlock:(dataRequestCompleted_t)completionBlock withFailedBlock:(dataRequestFailed_t)failedBlock;
- servicePath - String representation of the REST URL
- httpBodyInfo - Name/Value pairs for the type being MERGEed
- withCompletionBlock - **dataRequestCompleted_t** that is called when the request completes
- withFailedBlock - **dataRequestFailed_t** that is called if the request fails


*submitDELETERequest:withCompletionBlock:withFailedBlock:*

	- (void) submitDELETERequest:(NSString *)servicePath withCompletionBlock:(dataRequestCompleted_t)completionBlock withFailedBlock:(dataRequestFailed_t)failedBlock;
- servicePath - String representation of the REST URL
- withCompletionBlock - **dataRequestCompleted_t** that is called when the request completes
- withFailedBlock - **dataRequestFailed_t** that is called if the request fails


##Class Methods

*operationForGETRequest:*

	+ (BZODataRequest*) operationForGETRequest:(NSString*)servicePath;
- servicePath - String representation of the REST URL
- Returns a *BZODataRequest as an NSOperation

*operationForPOSTRequest:httpBodyInfo:*

	+ (BZODataRequest*) operationForPOSTRequest:(NSString*)servicePath httpBodyInfo:(NSDictionary*)httpBodyInfo;
- servicePath - String representation of the REST URL
- httpBodyInfo - Name/Value pairs for the type being POSTed
- Returns a *BZODataRequest as an NSOperation

*operationForMERGERequest:httpBodyInfo:*

	+ (BZODataRequest*) operationForMERGERequest:(NSString*)servicePath httpBodyInfo:(NSDictionary*)httpBodyInfo;
- servicePath - String representation of the REST URL
- httpBodyInfo - Name/Value pairs for the type being MERGEed
- Returns a *BZODataRequest as an NSOperation

*operationForDELETERequest:*

	+ (BZODataRequest*) operationForDELETERequest:(NSString*)servicePath;
- servicePath - String representation of the REST URL
- Returns a *BZODataRequest as an NSOperation

##Properties

*id\<BZODataRequestDelegate> delegate*

	@property (nonatomic, assign) id<BZODataRequestDelegate> delegate;
- **BZODataRequestDelegate**

*BOOL gzipEnabled*

	@property (nonatomic, assign) BOOL gzipEnabled;
- Whether or not to compress the payload using gzip
- defaults to **YES**

*NSString  *authUser*

	@property (nonatomic, copy) NSString *authUser;
- User Name for basic authentication 

*NSString *authPass*

	@property (nonatomic, copy) NSString *authPass;
- Password for basic authentication

*NSObject *context*

	@property (nonatomic, copy) NSObject *context;
- Object used for context tracking when using delegation.

##Protocols

***BZODataRequestDelegate***

	@protocol BZODataRequestDelegate <NSObject>

	@required
	- (void) request:(BZODataRequest*)request didReceiveResponse:(NSDictionary*)response;
	- (void) request:(BZODataRequest *)request didFailWithError:(NSError*) error;

	@end


*request:didReceiveResponse:*

	- (void) request:(BZODataRequest*)request didReceiveResponse:(NSDictionary*)response;
- request - The instance of the **BZODataRequest** calling the delegate
- response - The JSON Dictionary received by the **BZODataRequest** instance

*request:didFailWithError:*

	- (void) request:(BZODataRequest *)request didFailWithError:(NSError*) error;
- request - The instance of the **BZODataRequest** calling the delegate
- error - the **NSError** received by the **BZODataRequest** 

##Block Definitions

*dataRequestCompleted_t*

	typedef void (^dataRequestCompleted_t) (BZODataRequest *request, NSDictionary *response);
- request - The **BZODataRequest** that made the original request
- response - The JSON Dictionary returned by the service

*dataRequestFailed_t*

	typedef void (^dataRequestFailed_t) (BZODataRequest *request, NSError* error);
- request - The **BZODataRequest** that made the original request
- error - The **NSError** returned by the service

#*BZODataObjectMapper*

The BZODataObjectMapper handles populating NSObject instances with JSON Dictionary results

##Instance Methods

*populateObject:withJSONDictionary:*

	- (void) populateObject:(NSObject*)object withJSONDictionary:(NSDictionary*)jsonDictionary;
- object - The NSObject instance to be populated
- jsonDictionary - the source dictionary from which to get the data to populate
- *NOTE: Object property names must match the dictionary keys exactly*

*populateObject:withJSONDictionary:withFieldMapping:*

	- (void) populateObject:(NSObject *)object withJSONDictionary:(NSDictionary *)jsonDictionary withFieldMapping:(NSDictionary*)mapping;
- object - The NSObject instance to be populated
- jsonDictionary - the source dictionary from which to get the data to populate
- mapping - Dictionary to match disparate property names to dictionary keys

*extractEntitiesFromJSONDictionary:*

	- (NSArray*) extractEntitiesFromJSONDictionary:(NSDictionary*)jsonDictionary;
- jsonDictionary - the dictionary from which to extract entities
- *NOTE: Microsoft's JSON results have a root entity named ' d '.  This method extracts the inner results and returns them.*

#*BZODataRequestQueue*

The BZODataRequestQueue simply defaults the number of concurrent operations to four (4).

###Samples

#*MERGE Example using Delegation*

	@property(nonatomic, retain) BZODataRequest *odata;

	- (void)doMerge {
		NSMutableDictionary *params = [NSMutableDictionary dictionary];
		[params setValue:@"1" forKey:@"TransactionID"];
		[params setValue:[NSDate date] forKey:@"TransactionDate"];
	
		[self.odata submitMERGERequest:@"http://odataExample.apphb.com/DataServices.svc/Transactions(1)" httpBodyInfo:params];
	}

	- (void)request:(BZODataRequest *)request didReceiveResponse:(NSDictionary *)response {
		NSLog(@"JSON Response: %@", response);
		[request release];
	}

	- (void)request:(BZODataRequest *)request didFailWithError:(NSError *)error {   
		NSLog(@"Error:%@", error);
		[request release];
	}

#*GET Example using Blocks*

	BZODataRequest *request = [BZODataRequest new];

	[request submitGETRequest:@"http://myallowance-services.apphb.com/DataServices.svc/Users()$filter=UserId eq 1" 
		  withCompletionBlock:^(BZODataRequest *request, NSDictionary *response) {

			BZODataObjectMapper *mapper = [BZODataObjectMapper new];

			NSArray *entities = [mapper extractEntitiesFromJSONDictionary:response];

			for (NSDictionary *entity in entities) {

				DataServiceClientAppDelegate *appDelegate = (DataServiceClientAppDelegate*)[UIApplication sharedApplication].delegate;
				NSManagedObjectContext *managedObjectContext = appDelegate.managedObjectContext;
				User *user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:managedObjectContext];

				[mapper populateObject:user withJSONDictionary:entity];

				[appDelegate saveContext];
				[request release];
			}

		} 
		withFailedBlock:^(BZODataRequest *request, NSError *error) {

			NSLog(@"Submit GET WithFailedBlock: %@", error);
			[request release];
		}];

#*POST Example using NSOperation and NSOperationQueue*

	@property (nonatomic, retain) BZODataOperationQueue *odataQueue;

	- (void)doPost {
		NSMutableDictionary *params = [NSMutableDictionary dictionary];

		[params setValue:@"NewUserFromOperation" forKey:@"UserName"];
		[params setValue:@"1252355" forKey:@"AccessCode"];
		[params setValue:@"83484" forKey:@"RequestCode"];

		BZODataRequest *request = [BZODataRequest operationForPOSTRequest:@"http://odataExample.apphb.com/DataServices.svc/Users" httpBodyInfo:params];

		request.authuser = @"Me";
		request.authpass = @"mySuperSecretPassword";
		request.context = @"MY_POST_CONTEXT";
		request.delegate = self;

		[self.odataQueue addOperation:request];
	}

	- (void)request:(BZODataRequest *)request didReceiveResponse:(NSDictionary *)response {
		
		if ([request.context isEqualToString:@"MY_POST_CONTEXT"]) {
			NSLog(@"Got POST Response: %@", response);
		}

		[request release];
	}

	- (void)request:(BZODataRequest *)request didFailWithError:(NSError *)error {   
		NSLog(@"Error:%@", error);
		[request release];
	}