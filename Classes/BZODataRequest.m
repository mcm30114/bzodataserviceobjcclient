//
//  ODataRequest.m
//  DataServiceClient
//
//  Created by Joseph DeCarlo on 9/19/11.
//  Copyright 2011 Baked Ziti, LLC. All rights reserved.
//

#import "BZODataRequest.h"
#import "JSON.h"

@interface BZODataRequest()

@property (nonatomic, retain) NSNumber *httpStatusCode;
@property (nonatomic, retain) NSString *httpStatusMessage;
@property (nonatomic, retain) NSString *internalServicePath;

@property (nonatomic, assign) BOOL requestCompleted;
@property (nonatomic, retain) NSMutableData *receivedData;

@property (nonatomic, copy) dataRequestCompleted_t completionBlock;
@property (nonatomic, copy) dataRequestFailed_t failedBlock;

//Properties when used as NSOperation
@property (nonatomic, retain) NSArray *op_params;
@property (nonatomic, retain) NSString *op_submitMethodSelector;

- (NSString*) formattedStringFromDate:(NSDate*) date;
- (NSDictionary*) dictionaryWithFormattedDatesFromDictionary:(NSDictionary*)dictionary;
- (NSArray*) arrayWithFormattedDatesFromArray:(NSArray*)array;
- (BOOL) failureStatusFromStatusCode:(NSInteger)statusCode;

@end

@implementation BZODataRequest

@synthesize delegate;
@synthesize receivedData;
@synthesize gzipEnabled;
@synthesize requestCompleted;
@synthesize completionBlock = _completionBlock;
@synthesize failedBlock = _failedBlock;
@synthesize authUser;
@synthesize authPass;
@synthesize context;
@synthesize httpStatusCode;
@synthesize httpStatusMessage;
@synthesize internalServicePath;
@synthesize contextName;

//Properties for NSOperation
@synthesize op_params;
@synthesize op_submitMethodSelector;

//--------------------------------------------------------------------------------------------------------------
#pragma mark - Initialization and Deallocation

- (id)init {
    self = [super init];
    if (self) {
        
		self.gzipEnabled = YES;
    }
    return self;
}

- (void)dealloc {
    
	self.delegate = nil;
	self.receivedData = nil;
	self.op_params = nil;
	self.op_submitMethodSelector = nil;
    self.completionBlock = nil;
    self.failedBlock = nil;
	self.authUser = nil;
	self.authPass = nil;
    self.httpStatusCode = nil;
    self.httpStatusMessage = nil;
	self.internalServicePath = nil;
	self.contextName = nil;
	
    [super dealloc];
}

//--------------------------------------------------------------------------------------------------------------
#pragma mark - Properties

- (NSString *)servicePath {
	
	return self.internalServicePath;
}


//--------------------------------------------------------------------------------------------------------------
#pragma mark - ODataRequest

- (void) submitGETRequest:(NSString *)servicePath {
	
	[self submitGETRequest:servicePath withCompletionBlock:nil withFailedBlock:nil];
}

- (void) submitPOSTRequest:(NSString*)servicePath httpBodyItems:(NSArray*)httpBodyItems {
	
	[self submitPOSTRequest:servicePath httpBodyItems:httpBodyItems withCompletionBlock:nil withFailedBlock:nil];
}

- (void) submitPOSTRequest:(NSString *)servicePath httpBodyInfo:(NSDictionary *)httpBodyInfo {
	
    [self submitPOSTRequest:servicePath httpBodyInfo:httpBodyInfo withCompletionBlock:nil withFailedBlock:nil];
}

- (void) submitMERGERequest:(NSString *)servicePath httpBodyItems:(NSArray*)httpBodyItems {
	
	[self submitMERGERequest:servicePath httpBodyItems:httpBodyItems withCompletionBlock:nil withFailedBlock:nil];
}

- (void) submitMERGERequest:(NSString *)servicePath httpBodyInfo:(NSDictionary *)httpBodyInfo {
	
    [self submitMERGERequest:servicePath httpBodyInfo:httpBodyInfo withCompletionBlock:nil withFailedBlock:nil];
}

- (void)submitDELETERequest:(NSString *)servicePath {
	
	[self submitDELETERequest:servicePath withCompletionBlock:nil withFailedBlock:nil];
}

- (void) submitGETRequest:(NSString *)servicePath withCompletionBlock:(dataRequestCompleted_t)completionBlock withFailedBlock:(dataRequestFailed_t)failedBlock {
    
    self.completionBlock = completionBlock;
    self.failedBlock = failedBlock;
    self.httpStatusCode = nil;
    self.httpStatusMessage = nil;
	self.internalServicePath = servicePath;
    
    NSURL *url = [NSURL URLWithString:[servicePath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
	
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
	[request setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
	[request setTimeoutInterval:10.0];
	
	[request setHTTPMethod:@"GET"];
	
	if (self.gzipEnabled) {
		
		[request addValue:@"gzip" forHTTPHeaderField:@"Accepts-Encoding"];
	}
	
	//Setup GET Request Header
	[request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
	
	self.requestCompleted = NO;
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
	
	self.receivedData = [NSMutableData data];
	
	do {
		
		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
		
	} while (!self.requestCompleted);
    
	[connection release];
}
   
- (void) submitPOSTRequest:(NSString*)servicePath httpBodyItems:(NSArray*)httpBodyItems withCompletionBlock:(dataRequestCompleted_t)completionBlock withFailedBlock:(dataRequestFailed_t)failedBlock {
	
	[self submitPOSTRequest:servicePath httpBodyInfo:(NSDictionary*)httpBodyItems withCompletionBlock:completionBlock withFailedBlock:failedBlock];
}

- (void) submitPOSTRequest:(NSString *)servicePath httpBodyInfo:(NSDictionary *)httpBodyInfo withCompletionBlock:(dataRequestCompleted_t)completionBlock withFailedBlock:(dataRequestFailed_t)failedBlock {
    
    self.completionBlock = completionBlock;
    self.failedBlock = failedBlock;
    self.httpStatusCode = nil;
    self.httpStatusMessage = nil;
	self.internalServicePath = servicePath;
    
    NSURL *url = [NSURL URLWithString:[servicePath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
	
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
	[request setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
	[request setTimeoutInterval:10.0];
	
	NSString *httpBody = nil;
	
	if ([httpBodyInfo isKindOfClass:[NSDictionary class]]) {
		
		NSDictionary *dict = [self dictionaryWithFormattedDatesFromDictionary:httpBodyInfo];
		
		httpBody = [dict JSONRepresentation];
	}
	else if ([httpBodyInfo isKindOfClass:[NSArray class]]) {
		
		NSArray *arr = [self arrayWithFormattedDatesFromArray:(NSArray*)httpBodyInfo];
		
		httpBody =  [arr JSONRepresentation];
	}
		
	[request setHTTPBody:[httpBody dataUsingEncoding:NSUTF8StringEncoding]];
	
	[request setHTTPMethod:@"POST"];
	
	if (self.gzipEnabled) {
		
		[request addValue:@"gzip" forHTTPHeaderField:@"Accepts-Encoding"];
	}
	
	//Setup POST Request Header
	[request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
	[request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
	
	self.requestCompleted = NO;
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
	
	self.receivedData = [NSMutableData data];
	
	do {
		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
		
	} while (!self.requestCompleted);
	
	[connection release];
}

- (void)submitMERGERequest:(NSString *)servicePath httpBodyItems:(NSArray*) httpBodyItems withCompletionBlock:(dataRequestCompleted_t)completionBlock withFailedBlock:(dataRequestFailed_t)failedBlock {
	
	[self submitMERGERequest:servicePath httpBodyInfo:(NSDictionary*)httpBodyItems withCompletionBlock:completionBlock withFailedBlock:failedBlock];
}

- (void)submitMERGERequest:(NSString *)servicePath httpBodyInfo:(NSDictionary *)httpBodyInfo withCompletionBlock:(dataRequestCompleted_t)completionBlock withFailedBlock:(dataRequestFailed_t)failedBlock {
	
	self.completionBlock = completionBlock;
	self.failedBlock = failedBlock;
    self.httpStatusCode = nil;
    self.httpStatusMessage = nil;
	self.internalServicePath = nil;
	
	NSURL *url = [NSURL URLWithString:[servicePath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
	[request setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
	[request setTimeoutInterval:10.0];
    
    [request setHTTPMethod:@"POST"];
    
    if (self.gzipEnabled) {
        
        [request addValue:@"gzip" forHTTPHeaderField:@"Accepts-Encoding"];
    }
    
    //Setup MERGE Request Header
	[request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"MERGE" forHTTPHeaderField:@"X-HTTP-METHOD"];
    
    self.requestCompleted = NO;
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSDictionary *jsonDictionary = [self dictionaryWithFormattedDatesFromDictionary:httpBodyInfo];
	
	NSString *httpBody = [jsonDictionary JSONRepresentation];
	
	[request setHTTPBody:[httpBody dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    self.receivedData = [NSMutableData data];;
	
	do {
		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
		
	} while (!self.requestCompleted);
	
	[connection release];
}

- (void)submitDELETERequest:(NSString *)servicePath withCompletionBlock:(dataRequestCompleted_t)completionBlock withFailedBlock:(dataRequestFailed_t)failedBlock {
	
	self.completionBlock = completionBlock;
	self.failedBlock = failedBlock;
    self.httpStatusCode = nil;
    self.httpStatusMessage = nil;
	self.internalServicePath = servicePath;
	
	NSURL *url = [NSURL URLWithString:[servicePath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
	
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
	[request setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
	[request setTimeoutInterval:10.0];
	
	[request setHTTPMethod:@"DELETE"];
	
	if (self.gzipEnabled) {
		
		[request addValue:@"gzip" forHTTPHeaderField:@"Accepts-Encoding"];
	}
	
	//Setup GET Request Header
	[request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
	
	self.requestCompleted = NO;
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
	
	self.receivedData = [NSMutableData data];
	
	do {
		
		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
		
	} while (!self.requestCompleted);
    
	[connection release];
}

//--------------------------------------------------------------------------------------------------------------
#pragma mark - Class Methods

+ (BZODataRequest *)operationForGETRequest:(NSString *)servicePath {
	
	BZODataRequest *request = [BZODataRequest new];
	
	request.op_params = [NSArray arrayWithObject:servicePath];
	
	request.op_submitMethodSelector = NSStringFromSelector(@selector(submitGETRequest:));
	
	return [request autorelease];
}

+ (BZODataRequest *)operationForPOSTRequest:(NSString*)servicePath httpBodyItems:(NSArray*)httpBodyItems {
	
	BZODataRequest *request = [BZODataRequest new];
	
	request.op_params = [NSArray arrayWithObjects:servicePath, httpBodyItems, nil];
	request.op_submitMethodSelector = NSStringFromSelector(@selector(submitPOSTRequest:httpBodyItems:));
	
	return  [request autorelease];
}

+ (BZODataRequest *)operationForPOSTRequest:(NSString *)servicePath httpBodyInfo:(NSDictionary *)httpBodyInfo {
	
	BZODataRequest *request = [BZODataRequest new];
	
	request.op_params = [NSArray arrayWithObjects:servicePath, httpBodyInfo, nil];
	request.op_submitMethodSelector = NSStringFromSelector(@selector(submitPOSTRequest:httpBodyInfo:));
	
	return  [request autorelease];
}

+ (BZODataRequest *)operationForMERGERequest:(NSString*)servicePath httpBodyItems:(NSArray*)httpBodyItems {
	
	BZODataRequest *request = [BZODataRequest new];
    
    request.op_params = [NSArray arrayWithObjects:servicePath, httpBodyItems, nil];
    request.op_submitMethodSelector = NSStringFromSelector(@selector(submitMERGERequest:httpBodyItems:));
    
    return [request autorelease];
}

+ (BZODataRequest*) operationForMERGERequest:(NSString *)servicePath httpBodyInfo:(NSDictionary *)httpBodyInfo {
    
    BZODataRequest *request = [BZODataRequest new];
    
    request.op_params = [NSArray arrayWithObjects:servicePath, httpBodyInfo, nil];
    request.op_submitMethodSelector = NSStringFromSelector(@selector(submitMERGERequest:httpBodyInfo:));
    
    return [request autorelease];
}

+ (BZODataRequest *)operationForDELETERequest:(NSString *)servicePath {
	
	BZODataRequest *request = [BZODataRequest new];
	
	request.op_params = [NSArray arrayWithObject:servicePath];
	request.op_submitMethodSelector = NSStringFromSelector(@selector(submitDELETERequest:));
	
	return [request autorelease];
}

//--------------------------------------------------------------------------------------------------------------
#pragma mark - NSURLConnectionDelegate

- (void) connection:(NSURLConnection*) connection didReceiveResponse:(NSURLResponse *)response {
	
	NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
	
	NSInteger statusCode = [httpResponse statusCode];
    self.httpStatusCode = [NSNumber numberWithInt:statusCode];
	self.httpStatusMessage = [NSHTTPURLResponse localizedStringForStatusCode:statusCode];
	
//	NSLog(@"Received HTTP Response Code:%d Message:%@", statusCode, statusMessage);
    
    [self.delegate request:self didReceiveStatus:self.httpStatusMessage statusCode:statusCode statusIsConsideredFailure:[self failureStatusFromStatusCode:statusCode]];
	
}

- (void)connection:(NSURLConnection *) connection didReceiveData:(NSData *)data {
	
	[self.receivedData appendData:data];
}

- (NSCachedURLResponse *) connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
	
	return nil;
}

- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	
	self.receivedData = nil;
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	NSLog(@"Connection Failed: Error: %@ %@", [error localizedDescription], [[error userInfo]objectForKey:NSURLErrorFailingURLErrorKey]);
	
    if (self.failedBlock) {
        
        self.failedBlock(self, error);
    }
    
	if ([self.delegate respondsToSelector:@selector(request:didFailWithError:)]) {
		
		[self.delegate request:self didFailWithError:error];
	}
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection {
	
    if (self.httpStatusCode && [self failureStatusFromStatusCode:[self.httpStatusCode intValue]]) {
        
        NSLog(@"Not executing didReceiveResponse: because of bad status");
    }
    else {
        
        self.requestCompleted = YES;
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		
		NSDictionary *json = nil;
        
		if (self.receivedData) {
			
			NSString *dataString = [[[NSString alloc] initWithData:self.receivedData encoding:NSUTF8StringEncoding]autorelease];
			
			json = [dataString JSONValue];
		}
        
        if (self.completionBlock) {
            
            self.completionBlock(self, json);
        }
        
        if ([self.delegate respondsToSelector:@selector(request:didReceiveResponse:)]) {
            
            [self.delegate request:self didReceiveResponse:json];
        }
    }
}

- (void) connection:(NSURLConnection*)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    
    
}

- (void) connection:(NSURLConnection*)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    
    // only BASIC HTTP Auth supported/tested (iOS supports more, but until we test...)
	if ([[challenge protectionSpace] authenticationMethod] != NSURLAuthenticationMethodHTTPBasic) {
		return ;
	}
	
    if ([challenge previousFailureCount] == 0) {
        NSURLCredential *newCredential;
		if (self.authUser && self.authPass) {
			newCredential = [NSURLCredential credentialWithUser:self.authUser
													   password:self.authPass
													persistence:NSURLCredentialPersistenceNone];
			[[challenge sender] useCredential:newCredential
				   forAuthenticationChallenge:challenge];			
		}
		else {
			[[challenge sender] cancelAuthenticationChallenge:challenge];
		}
		
    } else {
        [[challenge sender] cancelAuthenticationChallenge:challenge];
    }
	
}
//connection:canAuthenticateAgainstProtectionSpace:

- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
	
	// only BASIC HTTP Auth supported/tested (iOS supports more, but until we test...)
	if ([[challenge protectionSpace] authenticationMethod] != NSURLAuthenticationMethodHTTPBasic) {
		return ;
	}
	
    if ([challenge previousFailureCount] == 0) {
        NSURLCredential *newCredential;
		if (self.authUser && self.authPass) {
			newCredential = [NSURLCredential credentialWithUser:self.authUser
													   password:self.authPass
													persistence:NSURLCredentialPersistenceNone];
			[[challenge sender] useCredential:newCredential
				   forAuthenticationChallenge:challenge];			
		}
		else {
			[[challenge sender] cancelAuthenticationChallenge:challenge];
		}
		
    } else {
        [[challenge sender] cancelAuthenticationChallenge:challenge];
    }
	
	
}

//--------------------------------------------------------------------------------------------------------------
#pragma mark - NSOperation

-(void) main {
	
	NSAssert(self.op_submitMethodSelector != nil, @"Must use operation operationForGETRequest or operationForPOSTRequest to create ODataRequest");
	
	SEL selector = NSSelectorFromString(self.op_submitMethodSelector);
	
	int paramterCount = [self.op_params count];
	
	if (paramterCount == 1) {
		
		[self performSelector:selector withObject:[self.op_params objectAtIndex:0]];
	}
	else if (paramterCount == 2) {
		
		[self performSelector:selector withObject:[self.op_params objectAtIndex:0] withObject:[self.op_params objectAtIndex:1]];
	}
}

//--------------------------------------------------------------------------------------------------------------
#pragma mark - Utility Methods

- (NSString *) formattedStringFromDate:(NSDate *)date {
	
	NSDateFormatter *formatter = [[[NSDateFormatter alloc]init] autorelease];
	
	[formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSSSSS"];
	
	return [formatter stringFromDate:date];
}

- (NSDictionary*) dictionaryWithFormattedDatesFromDictionary:(NSDictionary *)dictionary {
	
	NSMutableDictionary *returnValue = [NSMutableDictionary dictionaryWithDictionary:dictionary];
	
	for (id key in dictionary) {
		
		id value = [dictionary objectForKey:key];
		
		if ([value isKindOfClass:[NSDate class]]) {
			
			NSDate *date = value;
			
			value = [self formattedStringFromDate:date];
			
			[returnValue setValue:value forKey:key];
		}
		else if ([value isKindOfClass:[NSArray class]]) {
			
			[returnValue setValue:[self arrayWithFormattedDatesFromArray:value] forKey:key];
		}
	}
	
	return returnValue;
}

- (NSArray*) arrayWithFormattedDatesFromArray:(NSArray*)array {
	
	NSMutableArray *returnValue = [NSMutableArray arrayWithArray:array];
	
	for (int i = 0; i < [array count]; ++i) {
		
		id value = [array objectAtIndex:i];
		
		if ([value isKindOfClass:[NSDate class]]) {
			
			NSDate *date = value;
			
			value = [self formattedStringFromDate:date];
		
			[returnValue replaceObjectAtIndex:i withObject:value];
		}
	}
	
	return returnValue;
}
					

- (BOOL)failureStatusFromStatusCode:(NSInteger)statusCode {
    
    BOOL failure = NO;
    
    //4xx and 5xx are considered failures..sometimes 3xx, but we'll consider them successful
    
    NSString *status = [[NSNumber numberWithInt:statusCode]stringValue];
    
    char c = [status characterAtIndex:0];
    
    if (c == '4' || c == '5') {
    
        failure = YES;
    }
    
    return failure;
}

@end
