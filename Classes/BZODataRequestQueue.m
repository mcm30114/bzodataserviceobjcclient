//
//  ODataRequestQueue.m
//  DataServiceClient
//
//  Created by Joseph DeCarlo on 9/19/11.
//  Copyright 2011 Baked Ziti, LLC. All rights reserved.
//

#import "BZODataRequestQueue.h"


@implementation BZODataRequestQueue

//--------------------------------------------------------------------------------------------------------------
#pragma mark - Initialization and Deallocation

- (id)init {
    self = [super init];
    if (self) {
        
		//Set the maximum amount of concurrent operations to 4
		//As they are all HTTP
		[self setMaxConcurrentOperationCount:4];
    }
    return self;
}


@end
