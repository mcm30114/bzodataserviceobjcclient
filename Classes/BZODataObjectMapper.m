//
//  BZODataObjectMapper.m
//  DataServiceClient
//
//  Created by Joseph DeCarlo on 9/20/11.
//  Copyright 2011 Baked Ziti, LLC. All rights reserved.
//

#import "BZODataObjectMapper.h"
#import "NSString+camelCase.h"
#import <objc/runtime.h>


@interface BZODataObjectMapper()

- (NSString *)object:(NSObject *)object propertyType:(NSString *)propertyName;
- (BOOL) object:(NSObject *)object property:(NSString *)propertyName typeofClass:(Class)class;
- (NSDate*) dateFromJSONSerializedDate:(NSString*)jsonDate;
- (NSDecimalNumber*) decimalFromJSONSerializedDecimal:(NSString*)jsonDecimal;

@end

@implementation BZODataObjectMapper

@synthesize delegate = _delegate;
@synthesize context = _context;

//--------------------------------------------------------------------------------------------------------------
#pragma mark - Initialization and Deallocation

- (id)init {
    
    self = [super init];
    if (self) {

    }
    
    return self;
}

- (void)dealloc {

	self.delegate = nil;
	self.context = nil;
	
    [super dealloc];
}

//--------------------------------------------------------------------------------------------------------------
#pragma mark - BZODataObjectMapper

- (void) populateObject:(NSObject*)object withJSONDictionary:(NSDictionary*)jsonDictionary {    
    
    [self populateObject:object withJSONDictionary:jsonDictionary withFieldMapping:nil];
}

- (NSArray*) extractEntitiesFromJSONDictionary:(NSDictionary*)jsonDictionary {
	
	NSArray *temp = [jsonDictionary objectForKey:@"d"];
	
	if (!temp) {
		temp = (NSArray*)jsonDictionary;
	}
	
	return temp;
}

- (void) populateObject:(NSObject *)object withJSONDictionary:(NSDictionary *)jsonDictionary withFieldMapping:(NSDictionary *)mapping {
	
	NSDictionary *tempDictionary = [jsonDictionary objectForKey:@"d"];
	
	if (tempDictionary) {
		
		jsonDictionary = tempDictionary;
	}
    
    for (id key in jsonDictionary) {
        
		NSLog(@"Found Key:%@", key);
		
        id value = [jsonDictionary objectForKey:key];
        
        id mappingKey = [mapping objectForKey:key];
        
        NSString *selectorString = nil;
        
        if (mappingKey) {
            
            key = mappingKey;
        }
        
        selectorString = [NSString stringWithFormat:@"set%@:", key];
        
        if ([self object:object property:key typeofClass:[NSDate class]]) {
            
            NSLog(@"Found Date");                
			
			if (value != [NSNull null]) {
				
				value = [self dateFromJSONSerializedDate:value];
			}
        }
        else if ([self object:object property:key typeofClass:[NSDecimalNumber class]]) {
            
            NSLog(@"Found Decimal");
			
			if (value != [NSNull null]) {
				
				value = [self decimalFromJSONSerializedDecimal:value];
			}
        }
		else if ([self object:object property:key typeofClass:[NSArray class]] || [self object:object property:key typeofClass:[NSSet class]]) {

			NSLog(@"Found array");
			
			if (value != [NSNull null]) {
				if ([self.delegate respondsToSelector:@selector(objectMapper:objectForCollectionWithName:fromObject:jsonDictionary:)]) {
					value = [self.delegate objectMapper:self objectForCollectionWithName:key fromObject:object jsonDictionary:value];
				}
				else {
					value = nil;
				}
			}
		}
        else if ([value isKindOfClass:[NSDictionary class]]) {
            if (value != [NSNull null]) {
                if ([self.delegate respondsToSelector:@selector(objectMapper:objectForType:withName:fromObject:jsonDictionary:)]) {
                    value = [self.delegate objectMapper:self objectForType:[self object:object propertyType:key] withName:key fromObject:object jsonDictionary:value];
                }
                else {
                    value = nil;
                }
            }
        }

        SEL selector = NSSelectorFromString(selectorString);
        
        if ([object respondsToSelector:selector]) {
            
            NSLog(@"Executed [%@ %@%@];",[object class], selectorString, value);
			
			if (value == [NSNull null]) {
				value = nil;
			}
            
            [object performSelector:selector withObject:value];
        }
		else {
			
			NSLog(@"Skipping Key:%@", key);
		}
    }

}

- (NSDictionary*) dictionaryWithObject:(NSObject *)object {
	
	return [self dictionaryWithObject:object ignoreFieldNames:nil];
}

- (NSDictionary*) dictionaryWithObject:(NSObject *)object ignoreFieldNames:(NSArray *)ignoreFields {
    
    return [self dictionaryWithObject:object ignoreFieldNames:ignoreFields mappingFields:nil];
}

- (NSDictionary*) dictionaryWithObject:(NSObject *)object ignoreFieldNames:(NSArray*)ignoreFields mappingFields:(NSDictionary*)mapping {
		
	NSMutableDictionary *props = [NSMutableDictionary dictionary];
	
	unsigned int outCount, i;
	
	objc_property_t *properties = class_copyPropertyList([object class], &outCount);
	
	for (i = 0; i < outCount; i++) {
		
		objc_property_t property = properties[i];
		
		NSString *propertyName = [NSString stringWithUTF8String:property_getName(property)];
		
		if ([ignoreFields containsObject:propertyName]) {
			continue;
		}
		
		id propertyValue = [object valueForKey:(NSString *)propertyName];
		if (propertyValue) {
         
            NSString *mappingName = [mapping objectForKey:propertyName];
            
            if (mappingName) {
                propertyName = mappingName;
            }
            
            [props setObject:propertyValue forKey:propertyName];
        }
	}
	
	free(properties);
	
	return props;
}

//--------------------------------------------------------------------------------------------------------------
#pragma mark - Utility Methods

- (NSString *)object:(NSObject *)object propertyType:(NSString *)propertyName {
	
    SEL selector = NSSelectorFromString(propertyName);
	
	if (![object respondsToSelector:selector]) {
		
		propertyName = [propertyName toCamelCase];
		selector = NSSelectorFromString(propertyName);
	}
    
    if ([object respondsToSelector:selector]) {

        objc_property_t theProperty = class_getProperty([object class], [propertyName UTF8String]);
        
        const char *propertyAttrs = property_getAttributes(theProperty);
        
        NSString *propertyType = [NSString stringWithCString:propertyAttrs encoding:NSUTF8StringEncoding];
        
        NSArray *propNameComponents = [propertyType componentsSeparatedByString:@"\""];
        
        return [propNameComponents objectAtIndex:1];
    }
    
    return nil;
}

- (BOOL) object:(NSObject *)object property:(NSString *)propertyName typeofClass:(Class)class {
    
    return [[self object:object propertyType:propertyName] isEqualToString:NSStringFromClass(class)];
//    BOOL isType = NO;
//	
////    propertyName = [propertyName toCamelCase];
//	
//    SEL selector = NSSelectorFromString(propertyName);
//
//    if ([object respondsToSelector:selector]) {
//        
//        NSString *propertyType = [self object:object propertyType:propertyName];
//
//        NSLog(@"propertyAttrs = %@", propertyType);
//        
//        NSString *classTypeString = NSStringFromClass(class);
//        
//        NSRange range = [propertyType rangeOfString:classTypeString];
//        
//        isType = (range.location != NSNotFound);
//    }
//
//    return isType;
}

- (NSDate*) dateFromJSONSerializedDate:(NSString *)jsonDate {
    
    // JSON Serialized Date Format:  /Date(1273884549593)/
    
	NSString *ticksString = [jsonDate stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"/Date()"]];
	
    // convert milliseconds to seconds
	NSTimeInterval unixTime = ([ticksString doubleValue] / 1000);
    
	NSDate *returnDate = [NSDate dateWithTimeIntervalSince1970:unixTime];
    
	return returnDate;
}

- (NSDecimalNumber*) decimalFromJSONSerializedDecimal:(NSString *)jsonDecimal {
    
    return [NSDecimalNumber decimalNumberWithString:jsonDecimal];
}

//--------------------------------------------------------------------------------------------------------------
#pragma mark - Properties

@end
