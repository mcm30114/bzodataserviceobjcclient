//
//  BZODataObjectMapper.h
//  DataServiceClient
//
//  Created by Joseph DeCarlo on 9/20/11.
//  Copyright 2011 Baked Ziti, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BZODataObjectMapperDelegate;

@interface BZODataObjectMapper : NSObject

@property (nonatomic, assign) id<BZODataObjectMapperDelegate> delegate;

@property (nonatomic, retain) NSObject *context;

- (void) populateObject:(NSObject*)object withJSONDictionary:(NSDictionary*)jsonDictionary;
- (void) populateObject:(NSObject *)object withJSONDictionary:(NSDictionary *)jsonDictionary withFieldMapping:(NSDictionary*)mapping;

- (NSArray*) extractEntitiesFromJSONDictionary:(NSDictionary*)jsonDictionary;

- (NSDictionary*) dictionaryWithObject:(NSObject*)object;
- (NSDictionary*) dictionaryWithObject:(NSObject *)object ignoreFieldNames:(NSArray*)ignoreFields;
- (NSDictionary*) dictionaryWithObject:(NSObject *)object ignoreFieldNames:(NSArray*)ignoreFields mappingFields:(NSDictionary*)mapping;

@end


@protocol BZODataObjectMapperDelegate <NSObject>

- (NSObject *)objectMapper:(BZODataObjectMapper *)mapper objectForType:(NSString *)type withName:(NSString *)relationship fromObject:(NSObject *)object jsonDictionary:(NSDictionary *)jsonDictionary;
- (NSObject *)objectMapper:(BZODataObjectMapper *)mapper objectForCollectionWithName:(NSString *)collectionName fromObject:(NSObject *)object jsonDictionary:(NSDictionary *)jsonDictionary;

@end