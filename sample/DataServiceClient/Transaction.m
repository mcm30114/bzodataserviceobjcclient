//
//  Transaction.m
//  DataServiceClient
//
//  Created by Joseph DeCarlo on 9/21/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "Transaction.h"


@implementation Transaction
@dynamic TransactionID;
@dynamic TransactionTypeID;
@dynamic Amount;
@dynamic TransactionDate;

@end
