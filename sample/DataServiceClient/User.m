//
//  User.m
//  DataServiceClient
//
//  Created by Joseph DeCarlo on 9/20/11.
//  Copyright (c) 2011 Baked Ziti, LLC. All rights reserved.
//

#import "User.h"


@implementation User
@dynamic UserID;
@dynamic UserName;
@dynamic AccessCode;
@dynamic RequestCode;

@end
