//
//  User.h
//  DataServiceClient
//
//  Created by Joseph DeCarlo on 9/20/11.
//  Copyright (c) 2011 Baked Ziti, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface User : NSManagedObject {
@private
}
@property (nonatomic, retain) NSNumber *UserID;
@property (nonatomic, retain) NSString *UserName;
@property (nonatomic, retain) NSString *AccessCode;
@property (nonatomic, retain) NSString *RequestCode;

@end
