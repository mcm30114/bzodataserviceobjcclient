//
//  TestViewController.h
//  DataServiceClient
//
//  Created by Joseph DeCarlo on 9/19/11.
//  Copyright 2011 Baked Ziti, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BZODataRequest.h"

@interface TestViewController : UIViewController <BZODataRequestDelegate> {
    
}

- (IBAction)testTouched:(id)sender;
- (IBAction)testPostTouched:(id)sender;
- (IBAction)testGETOpTouched:(id)sender;
- (IBAction)testPOSTOPTouched:(id)sender;
- (IBAction)testGETBlockTouched:(id)sender;
- (IBAction)testMapperTouched:(id)sender;
- (IBAction)addTransTouched:(id)sender;
- (IBAction)getTransTouched:(id)sender;
- (IBAction)testMERGETouched:(id)sender;

@end
